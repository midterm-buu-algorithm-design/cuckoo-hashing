package com.anurak;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) {
        
        int keys_1[] = { 20, 50, 53, 75, 100,
                67, 105, 3, 36, 39 };

        int n = keys_1.length;

        Hash.cuckoo(keys_1, n);

        int keys_2[] = { 20, 50, 53, 75, 100,
                67, 105, 3, 36, 39, 6 };

        int m = keys_2.length;

        Hash.cuckoo(keys_2, m);
    }
}
